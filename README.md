
# ProteoSynth binaries

This where you can find binaries for ProteoSynth.

## Getting started

First, download the Windows, Linux x64, or Mac OSX binary and put it anywhere in your path.

Make sure you can run the binary:

    $ ProteoSynth --help

You can trick ProteoSynth into creating a default configuration by first creating an empty JSON file (containing `{}`) with a name such
as `ps-config.json` then running the tool:

    $ ProteoSynth synth -c ps-config.json -o tmp.mzML

The command will fail because you haven't specified a FASTA file or serialized spectrum library, but you'll have a complete configuration
with all in `tmp-config.json`.

## Predictive models

ProteoSynth uses extremely simple predictive models to predict each peptide's retention time, charge state, and quantity, as well as to model measurement noise.

Models can be downloaded here:
- [RT prediction model weights](models/rt-model.csv)
- [Charge state prediction model weights](models/charge-model.csv)
- [Intensity prediction model weights](models/intensity-model.csv)
- [Noise prediction model weights](models/peak-noise-model.csv)

Edit the configuration file to specify the full or relative paths of these four files.

